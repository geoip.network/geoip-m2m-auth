import json
import re
from datetime import datetime
from os import environ, path

import nacl.pwhash
import nacl.signing
from nacl.encoding import URLSafeBase64Encoder
from flask import Flask, request
from pony.orm import db_session, select

from models import Credential, db

db.bind(provider=environ.get("DATABASE_PROVIDER"), user=environ.get("DATABASE_USER"), password=environ.get("DATABASE_PASSWORD"), host=environ.get("DATABASE_HOST"), database=environ.get("DATABASE_NAME"))

app = Flask(__name__)

if path.isfile("secrets/ed25519.priv"):
    with open("secrets/ed25519.priv", "rb") as f:
        sk = nacl.signing.SigningKey(f.read())
        pk = sk.verify_key
else:
    with open("secrets/ed25519.priv", "wb") as f:
        sk = nacl.signing.SigningKey.generate()
        f.write(sk.encode())
    with open("secrets/ed25519.pub", "wb") as f:
        pk = sk.verify_key
        f.write(pk.encode())


# Error handler
class AuthError(ValueError):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


@app.route("/v1.0/certs", methods=["GET"])
def get_certs():
    response = {
        1: {
            "public_key": pk.encode(
                nacl.signing.encoding.URLSafeBase64Encoder
            ).decode(),
            "issuer": environ.get("M2M_URL"),
            "refresh": 3600,
        }
    }
    return response, 200


@app.route("/v1.0/token", methods=["POST"])
def new_token():
    request_payload = request.json
    try:
        grants, audience, client_id = validate_request(request_payload)
    except AuthError as e:
        return {"error": e.error}, e.status_code
    if len(grants) > 0:
        access_token = create_jwt(
            environ.get("M2M_URL"),
            int(datetime.now().timestamp()) + 3600,
            audience,
            client_id,
            client_id,
            int(datetime.now().timestamp()),
            grants,
        )
        response_payload = {
            "access_token": access_token,
            "token_type": "Bearer",
            "expires_in": 3600,
            "scope": grants,
        }
        return response_payload, 200
    else:
        return {"error": "invalid audience"}, 401


def validate_request(payload: dict):
    audience_dirty = payload.get("audience", "")
    r1 = re.compile(r"https?://(?P<domain>((\w*)\.?)*)/?")
    if (match := r1.search(audience_dirty)) is not None:
        audience_dirty = match.group("domain")
    r2 = re.compile(r"(?P<clean>((\w*)-?\.?)*)")
    if (match := r2.search(audience_dirty)) is not None:
        audience = match.group("clean")
    else:
        raise AuthError("invalid audience", 401)
    grants = []
    with db_session():
        credential = Credential.get(name=payload.get("client_id"))
        if credential is None:
            raise AuthError("no such credential", 401)
        try:
            nacl.pwhash.verify(
                credential.key_hash, payload.get("client_secret").encode()
            )
        except nacl.exceptions.InvalidkeyError as e:
            raise AuthError("invalid secret", 401) from e
        grants = select(
            p.grant
            for role in credential.roles
            for p in role.permissions
            if p.service.service_url == audience
        )[:]
    if len(grants) < 1:
        raise AuthError("no matching claims", 401)
    return grants, audience, credential.client_name.decode()


def create_jwt(iss, exp, aud, sub, client_id, iat, grants):
    header = {
        "typ": "JWT",
        "alg": "ED25519",
        "kid": 1,
    }
    payload = {
        "iss": iss,
        "exp": exp,
        "aud": aud,
        "sub": sub,
        "client_id": client_id,
        "iat": iat,
        "scope": " ".join(grants),
    }
    header_enc = URLSafeBase64Encoder.encode(json.dumps(header).encode()).decode()
    payload_enc = URLSafeBase64Encoder.encode(json.dumps(payload).encode()).decode()
    signature = sk.sign(
        f"{header_enc}.{payload_enc}".encode(), encoder=URLSafeBase64Encoder
    ).signature
    signature_enc = signature.decode()
    return f"{header_enc}.{payload_enc}.{signature_enc}"


if __name__ == "__main__":
    app.run()
