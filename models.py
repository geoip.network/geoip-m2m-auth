from pony.orm import *

db = Database()


class Credential(db.Entity):
    uid = PrimaryKey(int, auto=True)
    client_name = Required(str, unique=True)
    key_hash = Required(bytes)
    roles = Set("Role")


class Role(db.Entity):
    uid = PrimaryKey(int, auto=True)
    role_name = Required(str, unique=True)
    credentials = Set(Credential)
    permissions = Set("Permission")


class Service(db.Entity):
    uid = PrimaryKey(int, auto=True)
    service_url = Required(str, unique=True)
    permissions = Set("Permission")


class Permission(db.Entity):
    uid = PrimaryKey(int, auto=True)
    service = Required(Service)
    roles = Set(Role)
    grant = Required(str)
