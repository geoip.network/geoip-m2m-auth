import json
from os import environ
from collections import defaultdict
from datetime import datetime

import click
import nacl.pwhash
import nacl.hash
import nacl.encoding
from pony.orm import db_session, commit, select

from models import Credential, db, Role, Permission, Service

db.bind(provider='postgres', user=environ.get("DATABASE_USER"), password=environ.get("DATABASE_PASSWORD"), host=environ.get("DATABASE_HOST"), database=environ.get("DATABASE_NAME"))


@click.group()
@click.pass_context
def cli(ctx):
    if ctx.invoked_subcommand is None:
        pass


@cli.group()
@click.pass_context
def client(ctx):
    if ctx.invoked_subcommand is None:
        pass


@cli.group()
@click.pass_context
def role(ctx):
    if ctx.invoked_subcommand is None:
        pass


@cli.group()
@click.pass_context
def permission(ctx):
    if ctx.invoked_subcommand is None:
        pass


@cli.group()
@click.pass_context
def service(ctx):
    if ctx.invoked_subcommand is None:
        pass


@client.command("add")
@click.argument("clientname")
def add_client(clientname):
    key = nacl.hash.sha256(
        f"{clientname}-{int(datetime.now().timestamp())}".encode(),
        nacl.encoding.Base64Encoder,
    )
    key_hash = nacl.pwhash.str(key)
    with db_session():
        Credential(client_name=clientname, key_hash=key_hash)
        commit()
    print(key.decode())


@client.command("assign_role")
@click.argument("client_name")
@click.argument("role_name")
def assign_role(client_name: str, role_name: str):
    if len(client_name) > 128:
        print("Supplied client name is too long")
        return
    if len(role_name) > 128:
        print("Supplied role name is too long")
        return
    with db_session():
        _role = Role.get(role_name=role_name)
        if _role is None:
            print("Supplied role name is not found")
            return
        cred = Credential.get(client_name=client_name)
        if cred is None:
            print("Supplied client name is not found")
            return
        cred.roles.add(role)
        commit()


@client.command("delete")
@click.argument("clientname")
def delete_client(clientname):
    with db_session():
        cred = Credential.get(client_name=clientname)
        if cred is None:
            print("Supplied client name is not found")
            return
        del cred
        commit()


@client.command("list")
@click.option("--verbose", is_flag=True)
@click.option("--tree", is_flag=True)
def list_client(verbose=False, tree=False):
    credential_tree = {}
    with db_session():
        for credential in Credential[:]:
            if tree:
                key, val = client_tree(credential)
                credential_tree[key] = val
            elif verbose:
                    print(
                        f"Credential(uid={credential.uid}, client_name={credential.client_name}, key_hash=**HIDDEN**)"
                    )
            else:
                print(credential.client_name)
    if tree:
        print(json.dumps(credential_tree, indent=4))


def client_tree(cred: Credential):
    label = f"{cred.client_name.decode()}"
    client_credential_tree = {"roles": dict()}
    for perm in select(perm for _role in cred.roles for perm in _role.permissions):
        client_credential_tree["roles"][perm.service.service_url] = perm.grant
    return label, client_credential_tree


@role.command("add")
@click.argument("role_name")
def add_role(role_name):
    if len(role_name) > 128:
        print("Supplied name too long")
        return
    with db_session():
        _role = Role(role_name=role_name)
        commit()


@role.command("assign_permission")
@click.argument("role_name")
@click.argument("permission_uid", type=int)
def assign_permission(role_name: str, permission_uid: int):
    if len(role_name) > 128:
        print("Supplied name too long")
        return
    with db_session():
        _role = Role.get(role_name=role_name)
        if _role is None:
            print("Supplied role is not found")
            return
        perm = Permission.get(uid=permission_uid)
        if perm is None:
            print("Supplied permission uid is not found")
            return
        _role.permissions.add(perm)
        commit()


@role.command("list")
@click.option("--verbose", is_flag=True)
def list_roles(verbose=False):
    with db_session():
        for _role in Role[:]:
            if verbose:
                print(f"Role(uid={_role.uid}, role_name={_role.role_name.decode()})")
            else:
                print(_role.role_name.decode())


@service.command("add")
@click.argument("service_url")
def add_service(service_url):
    if len(service_url) > 256:
        print("Supplied URL too long")
        return
    with db_session():
        svc = Service(service_url=service_url)
        commit()


@service.command("list")
@click.option("--verbose", is_flag=True)
def list_services(verbose=False):
    with db_session():
        for svc in Service[:]:
            if verbose:
                print(f"Service(uid={svc.uid}, service_url={svc.service_url.decode()})")
            else:
                print(svc.service_url.decode())


@permission.command("add")
@click.argument("service_url")
@click.argument("grant")
def add_permission(service_url, grant):
    if len(service_url) > 256:
        print("Supplied URL too long")
        return
    if len(grant) > 256:
        print("Supplied grant too long")
        return
    with db_session():
        svc = Service.get(service_url=service_url)
        if svc is None:
            print("Supplied URL is not found")
            return
        perm = Permission(service=svc, grant=grant)
        commit()


@permission.command("delete")
@click.argument("permission_uid", type=int)
def delete_permission(permission_uid):
    with db_session():
        perm = Permission.get(uid=permission_uid)
        if perm is None:
            print("Supplied permission is not found")
        del perm
        commit()


@permission.command("list")
@click.option("--verbose", is_flag=True)
def list_permissions(verbose=False):
    with db_session():
        for perm in Permission[:]:
            svc = perm.service
            if verbose:
                print(
                    f"Permission(uid={perm.uid}, service_url={svc.service_url.decode()}, grant={perm.grant.decode()})"
                )
            else:
                print(f"{svc.service_url.decode()}:{perm.grant.decode()}")


if __name__ == "__main__":
    cli()
